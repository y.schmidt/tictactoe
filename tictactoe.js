let xIsNext, gameOver;  // Status welcher Spieler an der Reihe ist und ob das Spiel vorbei ist
const board = [];       // Status des Spielfelds
const history = [];     // Array mit Spielhistorie
initGame();             // Methodenaufruf: Die Methode initGame() wird aufgerufen

/**
 * Setzt alle Variablen und Historien wieder zurück
 */
function initGame() {
  /**
   * Die Schleife läuft so lange, bis die Bedingung (i < 9) nicht mehr wahr ist
   * Jeder Schleifendurchgang erhährt die Variable i um eins (i++)
   */
  for(let i = 0; i < 9; i++) {
    board[i] = null;
    document.getElementById(i.toString()).innerHTML = '';
  }
  gameOver = false;
  xIsNext = true;
  history.splice(0, history.length);
  document.getElementById('history').innerHTML = '';
}

function handleClick(squareId) {
  if(!gameOver && board[squareId] == null) {           // Spiel wird nur fortgeführt, wenn Spiel nicht vorbei und geklicktes Feld nicht gefüllt
    history.push(Object.assign({}, board));     // Aktueller Status wird in History geschrieben
    const element = document.getElementById(squareId);
    // Wenn x an der Reihe ist, dann wird X geschrieben, ansonsten 0
    if(xIsNext) {
      board[squareId] = 'X';
      element.innerHTML = 'X';
    } else {
      board[squareId] = '0';
      element.innerHTML = '0';
    }
    checkWinner();          // Funktion die prüft, ob es einen Gewinner gibt
    renderHistoryButton();  // Button wird gerendert
  }
}

/**
 * Setzt Status, wer an der Reihe ist
 */
function setStatus() {
  const status = document.getElementById('status');
  status.innerHTML = xIsNext ? '0' : 'X';
}

/**
 * Prüft, ob es einen Gewinner gibt
 */
function checkWinner() {
  // Array lines enthält alle Kombinationen die zum Gewinnen führen
  const lines = [
    [0, 1, 2],
    [3, 4, 5],
    [6, 7, 8],
    [0, 3, 6],
    [1, 4, 7],
    [2, 5, 8],
    [0, 4, 8],
    [2, 4, 6]
  ];
  // for-Schleife durchläuft alle Werte des Arrays lines. Die Werte werden temporär in line geschrieben
  for(let line of lines) {
    const [a, b, c] = line; // die Werte von line werden in a, b, c geeschrieben
    /**
     * wenn a = b und a = c, dann ist b = c
     * Es wird das Board geprüft, ob in drei relevanten Feldern die gleichen
     * Werte enthalten sind. Falls ja gibt es einen Gewinner
     */

    if(board[a] && board[a] === board[b] && board[a] === board[c]){
      document.getElementById('status').innerHTML = 'Winner is: ' + board[a];
      gameOver = true;
      return;
    }
  }
  setStatus();
  xIsNext = !xIsNext; // Jeweils der andere Spieler wird an die Reihe genommen
}

/**
 * Erstellt Historien-Button
 */

function renderHistoryButton() {
  const button = document.createElement('button'); // Erstellt Element
  button.innerHTML = 'History Button ' + history.length;    // Schreibt Inhalt
  button.id = 'history' + (history.length-1);               // Setzt ID
  button.className = 'historyButton';                       // Setzt CSS-Klasse
  button.addEventListener('click', () => {     // Setzt click-Listener
    setHistory(button.id.slice(-1));
  });
  document.getElementById('history').appendChild(button); // Fügt Button hinzu
}

/**
 * Setzt Historie auf den geklickten Stand
 * @param id
 */
function setHistory(id) {
  gameOver = false;
  for(let i = 0; i < board.length; i++){
    board[i] = history[id][i];
    document.getElementById(i).innerHTML = board[i];
  }
}
